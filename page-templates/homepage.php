<?php

/**
 * Template Name: Página Inicial
 *
 * Template da página inicial do website.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();
$container = get_theme_mod('understrap_container_type');
?>

<div class="homepage-wrapper" id="content">

	<!-- Hero -->
	<div class="homepage-hero py-0">
		<div class="container">
			<div class="col-md-6 px-0 text-dark">
				<div class="h-100 p-5" style="background-color: rgba(255, 255, 255, 0.4)">

					<div class="my-5">
						<h1 class="heading-title text-uppercase position-relative"><?php echo bloginfo('name') ?></h1>
						<p class="my-4">Os nossos conhecimentos são a reunião do raciocínio e experiência de numerosas mentes.</p>
						<p class="mb-0"><a href="#" class="btn btn-lg btn-outline-primary font-weight-normal text-uppercase p-3">CONHECER</a></p>
					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- Section: Área de Atuaçao -->
	<section id="area-de-atuacao" class="py-5">
		<div class="<?php echo esc_attr($container); ?>">

			<!-- Serviços Heading Title -->
			<div class="row text-center mb-4">
				<div class="col-12">
					<i class="heading-title-icon"></i>
				</div>
				<div class="col-12">
					<h3 class="heading-title text-uppercase">Áreas de Atuação</h3>
				</div>
			</div>

			<!-- Serviços Desc; Columns -->
			<div class="row py-4 icons-section-img text-center">

				<div class="col-md-3 col-6 mb-5 mb-sm-0">
					<figure class="mb-4 mb-sm-5">
						<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/icon_atuacao_01.png" alt="" />
					</figure>
					<div class="text-center">
						<h5 class="card-title">Atuação 01</h5>
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum rhoncus pharetra.</p>
					</div>
				</div>

				<div class="col-md-3 col-6 mb-5 mb-sm-0">
					<figure class="mb-4 mb-sm-5">
						<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/icon_atuacao_02.png" alt="" />
					</figure>
					<div class="text-center">
						<h5 class="card-title">Atuação 01</h5>
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum rhoncus pharetra.</p>
					</div>
				</div>

				<div class="col-md-3 col-6">
					<figure class="mb-4 mb-sm-5">
						<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/icon_atuacao_03.png" alt="" />
					</figure>
					<div class="text-center">
						<h5 class="card-title">Atuação 01</h5>
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum rhoncus pharetra.</p>
					</div>
				</div>

				<div class="col-md-3 col-6">
					<figure class="mb-4 mb-sm-5">
						<img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/icon_atuacao_04.png" alt="" />
					</figure>
					<div class="text-center">
						<h5 class="card-title">Atuação 01</h5>
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum rhoncus pharetra.</p>
					</div>
				</div>

			</div>

			<div class="mt-5 text-center">
				<button class="btn btn-lg btn-warning py-3 px-4">
					Saber mais
				</button>
			</div>
		</div>
	</section>

	<!-- Section: Citação #1 -->
	<section id="sitacao-heading" class="">
		<div class="section-bg">
			<div class="<?php echo esc_attr($container); ?>" style="max-width: 50em;margin: 0 auto;">
				<blockquote class="heading-title mb-0 text-white mx-4 mx-lg-0">
					<h4 class="citacao-title text-center mb-4">
						“Não há um único dos nossos actos que, ao criarem o homem que queremos ser, não crie ao mesmo tempo uma imagem do homem tal como estimamos que ele deve ser.”
					</h4>
					<p class="mb-0 text-right text-uppercase">JEAN-PAUL SARTRE</p>
				</blockquote>
			</div>
		</div>
	</section>

	<!-- Section: Quem Somos -->
	<section id="quem-somos" class="py-5">
		<div class="<?php echo esc_attr($container); ?>">
			<!-- Serviços Heading Title -->
			<div class="row text-center mb-4">
				<div class="col-12">
					<i class="heading-title-icon"></i>
				</div>
				<div class="col-12">
					<h3 class="heading-title text-uppercase">Quem Somos</h3>
					<div class="row">
						<div class="col-lg-8 pr-lg-0">
							<div id="quemSomos--avatar" class="d-flex flex-row justify-content-center justify-content-lg-start align-items-center p-0 bg-white">
								<div>
									<figure>
										<img src="<?php echo get_template_directory_uri() ?>/img/equipe/pessoa1.png" alt="Sigurd">
										<figcaption class="figure-caption avatar-nome">Sigurd</figcaption>
									</figure>
								</div>
								<div>
									<figure>
										<img src="<?php echo get_template_directory_uri() ?>/img/equipe/pessoa2.png" alt="Laura">
										<figcaption class="figure-caption avatar-nome">Laura</figcaption>
									</figure>
								</div>
								<div>
									<figure>
										<img src="<?php echo get_template_directory_uri() ?>/img/equipe/pessoa3.png" alt="Marco">
										<figcaption class="figure-caption avatar-nome">Marco</figcaption>
									</figure>
								</div>
								<div>
									<figure>
										<img src="<?php echo get_template_directory_uri() ?>/img/equipe/pessoa4.png" alt="Rebecca">
										<figcaption class="figure-caption avatar-nome">Rebecca</figcaption>
									</figure>
								</div>
							</div>
						</div>
						<div id="avatar-details" class="col-lg-4 pl-lg-0">
							<div class="h-100 p-4 p-md-5 bg-white text-left">
								<h4 id="avatar-details--name">Sigurd</h4>
								<p id="avatar-details--summary">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut auctor, ex quis sollicitudin fringilla.
								</p>
								<button class="btn btn-outline-primary">
									Ver currículo completo
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Section: suporte -->
	<section id="suporte" class="bg-info py-5">
		<div class="<?php echo esc_attr($container); ?>" style="max-width: 60em;margin: 0 auto;">
			<div class="row align-items-center text-center text-md-left">
				<div class="col-sm-8 mb-4 mb-sm-0">
					<div class="px-5 px-lg-0">
						<h5 class="text-white mb-3">Como a LAW FIRM pode te ajudar?</h5>
						<div style="line-height: 1.2">
							<p class="mb-0 text-white">Dúvidas sobre o seu caso? Entre em contato.</p>
							<p class="mb-0 text-white">Reference site about Lorem Ipsum, giving information on its origins.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<button class="btn btn-lg btn-warning">Consulte Agora</button>
				</div>
			</div>
		</div>
	</section>

	<!-- Section: Artigos/Blog -->
	<section id="artigos" class="py-5">
		<div class="<?php echo esc_attr($container); ?>">
			<!-- Serviços Heading Title -->
			<div class="row text-center mb-4">
				<div class="col-12">
					<i class="heading-title-icon"></i>
				</div>
				<div class="col-12">
					<h3 class="heading-title text-uppercase">Artigos</h3>
				</div>
			</div>
			<div class="row">
				<?php
				$wp_      = $wp_query;
				$wp_query = null;
				$wp_query = new wp_query();
				$wp_query->query('posts_per_page=8' . '&paged=' . $paged);
				while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<div class="col-md-4 h-100 mb-4 mb-sm-0">
						<div class="site-posts--featured-image bg-white">
							<?php echo get_the_post_thumbnail($post->ID) ?>
							<h6 class="bg-info text-white" style="padding: 0.75rem 1.5rem"><?php the_title(); ?></h6>
							<div class="card-body">
								<p class="small text-muted"><?php echo get_the_date() ?></p>
								<p class="mb-0"><?php echo get_excerpt() ?></p>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</div>
			<div class="mt-5 text-center">
				<a href="<?php echo site_url() ?>/artigos" class="btn btn-lg btn-warning py-3 px-4">
					Ver mais artigos
				</a>
			</div>
		</div>
	</section>

	<!-- Section: Inscrição-n/Newsletter -->
	<section id="inscricao-newsletter" class="bg-primary">
		<div class="<?php echo esc_attr($container); ?>">
			<div class="row align-items-center">
				<div class="newsletter-left--text col-md-5 mb-3 mb-sm-0">
					<div class="py-4">
						<h6 class="mb-0 pr-lg-5 text-white text-center text-md-left">
							Receba artigos sobre velit in suscipit pharetra erat turpis tempor tortor
						</h6>
					</div>
				</div>
				<div class="newsletter-left--form col-md-7">
					<div class="input-group mb-3 mb-sm-0 w-100">
						<input type="text" class="form-control" placeholder="Digite o seu e-mail..." aria-label="Digite o seu e-mail...">
						<div class="input-group-append">
							<button class="btn btn-info" onclick="return (window.alert('Obrigado por se inscraver!'))" type="button">Enviar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Section: Mapa/Contato -->
	<section id="contato" class="py-5">
		<div class="<?php echo esc_attr($container); ?>">
			<!-- Serviços Heading Title -->
			<div class="row text-center mb-4">
				<div class="col-12">
					<i class="heading-title-icon"></i>
				</div>
				<div class="col-12">
					<h3 class="heading-title text-uppercase">Onde Estamos</h3>
					<article class="bg-white p-1 p-md-3">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14131.415821338356!2d-48.50626969107798!3d-27.69090838208058!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x952738544297518f%3A0xeb1ac232bfa23e11!2zSW5DdWNhIFNvbHXDp8O1ZXMgVGVjbm9sw7NnaWNhcw!5e0!3m2!1spt-BR!2sbr!4v1569155564069!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					</article>
				</div>
			</div>
		</div>
	</section>

</div>

<?php get_footer(); ?>