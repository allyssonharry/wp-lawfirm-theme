<?php

/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

$container = get_theme_mod('understrap_container_type');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<style>
		@import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css');
		@import url('https://fonts.googleapis.com/css?family=Raleway:700&display=swap');
		@import url('https://fonts.googleapis.com/css?family=Open+Sans&display=swap');
	</style>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php do_action('wp_body_open'); ?>
	<div class="site" id="page">
		<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

			<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e('Skip to content', 'understrap'); ?></a>

			<!-- Topbar -->
			<div class="topbar bg-primary">
				<?php if ('container' == $container) : ?>
					<div class="container">
						<div class="d-flex flex-column flex-sm-row py-2 justify-content-between align-items-center">
							<p class="m-0 p-0 small text-white mb-2 mb-sm-0"><i class="fas fa-phone"></i> (48) 3344-9944 | (48) 3564-4499</p>
							<nav class="nav">
								<a href="#" rel="noopener"><i class="fab fa-facebook"></i></a>
								<a href="#" rel="noopener"><i class="fab fa-twitter"></i></a>
								<a href="#" rel="noopener"><i class="fab fa-linkedin"></i></a>
							</nav>
						</div>
					</div>
				<?php endif; ?>
			</div>

			<nav class="navbar navbar-expand-md navbar-light bg-transparent">

				<?php if ('container' == $container) : ?>
					<div class="container">
					<?php endif; ?>
					<!-- Your site title as branding in the menu -->
					<?php if (!has_custom_logo()) { ?>
						<?php if (is_front_page() && is_home()) : ?>
							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" itemprop="url"><?php bloginfo('name'); ?></a></h1>
						<?php else : ?>
							<a class="navbar-brand" rel="home" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" itemprop="url"><?php bloginfo('name'); ?></a>
						<?php endif; ?>
					<?php } else {
						the_custom_logo();
					} ?>
					<!-- end custom logo -->

					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e('Toggle navigation', 'understrap'); ?>">
						<span class="navbar-toggler-icon"></span>
					</button>

					<!-- The WordPress Menu goes here -->
					<?php wp_nav_menu(
						array(
							'theme_location'  => 'primary',
							'container_class' => 'collapse navbar-collapse',
							'container_id'    => 'navbarNavDropdown',
							'menu_class'      => 'main-menu--site navbar-nav ml-auto',
							'fallback_cb'     => '',
							'menu_id'         => 'main-menu',
							'depth'           => 2,
							'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
						)
					); ?>
					<?php if ('container' == $container) : ?>
					</div><!-- .container -->
				<?php endif; ?>

			</nav><!-- .site-navigation -->

		</div><!-- #wrapper-navbar end -->

		<!-- Site Search -->
		<div class="main-menu--searchWrap" style="display: none">
			<?php //get_template_part('global-templates/hero'); 
			?>
			<div class="container py-2">
				<form method="get" id="searchform" action="<?php site_url(); ?>" role="search">
					<label class="sr-only" for="s">Pesquisar</label>
					<div class="input-group">
						<input class="field form-control" id="s" name="s" type="text" placeholder="Digite aqui..." value="">
						<span class="input-group-append">
							<input class="submit btn btn-primary" id="searchsubmit" name="submit" type="submit" value="Pesquisar">
						</span>
					</div>
				</form>
			</div>
		</div>