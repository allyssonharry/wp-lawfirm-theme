<?php

/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

$container = get_theme_mod('understrap_container_type');
?>

<footer class="site-footer bg-dark py-5">
	<div class="<?php echo esc_attr($container); ?> px-4 px-sm-0">
		<div class="row">
			<div class="col-md-3">
				<div class="site-footer--logo h-100 mb-5 mb-sm-0">
					<img src="<?php echo get_template_directory_uri() ?>/img/logo_white.png" alt="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="site-footer--address">

					<div class="mb-4 mb-sm-5">
						<p class="text-uppercase"><?php echo bloginfo('name') ?></p>
						<div>
							Rua Padre Roma, 482 - Centro<br />
							Florianópolis - Santa Catarina<br />
							(48) 3344-9944<br />
							(48) 3564-9944<br />
							oi@lawfirm.com
						</div>
					</div>

					<div class="site-footer--address-social mb-5 mb-sm-0">
						<p class="text-uppercase">Redes Sociais</p>
						<nav class="nav">
							<a href="#" rel="noopener"><i class="fab fa-facebook"></i></a>
							<a href="#" rel="noopener"><i class="fab fa-twitter"></i></a>
							<a href="#" rel="noopener"><i class="fab fa-linkedin"></i></a>
						</nav>
					</div>

				</div>
			</div>
			<div class="col-md-5">
				<div class="site-footer--form text-white">
					<p class="text-uppercase">Entre em contato</p>
					<div class="text-right">
						<?php echo do_shortcode('[contact-form-7 id="5" title="Contato"]') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- Scroll to top -->
<div class="scrollTop">
	<span><a class="small" href=""><i class="fas fa-chevron-up"></i></a></span>
</div>

<?php get_template_part('sidebar-templates/sidebar', 'footerfull'); ?>

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

<script>
	// NiceScroll
	jQuery('a[href*="#"]')
		.not('[href="#"]')
		.not('[href="#0"]')
		.click(function(event) {
			if (
				location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
				location.hostname == this.hostname
			) {
				var target = jQuery(this.hash);
				target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					// Only prevent default if animation is actually gonna happen
					event.preventDefault();
					jQuery('html, body').animate({
						scrollTop: target.offset().top
					}, 800, function() {
						// Callback after animation
						// Must change focus!
						var $target = jQuery(target);
						$target.focus();
						if ($target.is(":focus")) { // Checking if the target was focused
							return false;
						} else {
							$target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
							$target.focus(); // Set focus again
						};
					});
				}
			}
		});

	// Avatars
	jQuery(document).ready(function() {
		jQuery("#quemSomos--avatar img").on("click", function() {
			var getAvatarName = jQuery(this).attr("alt");
			jQuery("#quemSomos--avatar img").removeClass("active");
			jQuery(this).addClass("active");
			var getActiveClass = jQuery(this).attr("class");
			if (getActiveClass === 'active') {
				jQuery(".avatar-nome").css("display", "block");
			}
			jQuery("#avatar-details--name").text(getAvatarName);
		});
	})

	// Search
	jQuery(document).ready(function() {
		jQuery(".main-menu--buscar").on("click", function() {
			jQuery(".main-menu--searchWrap").toggle();
		});
	});

	// Scroll to top
	// BY KAREN GRIGORYAN

	jQuery(document).ready(function() {
		/******************************
		    BOTTOM SCROLL TOP BUTTON
		 ******************************/

		// declare variable
		var scrollTop = jQuery(".scrollTop");

		jQuery(window).scroll(function() {
			// declare variable
			var topPos = jQuery(this).scrollTop();
			// if user scrolls down - show scroll to top button
			if (topPos > 600) {
				jQuery(scrollTop).css("opacity", "1");
			} else {
				jQuery(scrollTop).css("opacity", "0");
			}
		});
		//Click event to scroll to top
		jQuery(scrollTop).click(function() {
			jQuery('html, body').animate({
				scrollTop: 0
			}, 800);
			return false;

		});
	});
</script>

</body>

</html>