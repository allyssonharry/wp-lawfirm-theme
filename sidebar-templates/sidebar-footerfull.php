<?php

/**
 * Sidebar setup for footer full.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

$container = get_theme_mod('understrap_container_type');

?>

<?php if (is_active_sidebar('footerfull')) : ?>
	<!-- ******************* The Footer Full-width Widget Area ******************* -->
	<div class="py-1" id="wrapper-footer-full">
		<div class="<?php echo esc_attr($container); ?>" tabindex="-1">
			<div class="main-menu--footer">
				<?php dynamic_sidebar('footerfull'); ?>
			</div>
		</div>
	</div><!-- #wrapper-footer-full -->

<?php endif; ?>